import React,{Component} from "react";

// import logo from './logo.svg';
import './App.css';

//React Video player stuff

import "./video-react.css"; // import css
import { Player, BigPlayButton  } from 'video-react';
import { Button,AppBar,Typography,Toolbar } from '@material-ui/core';
//socketIOClient init
import socketIOClient from 'socket.io-client'
var room="CombiStream"
const socket = socketIOClient('https://safe-bastion-45297.herokuapp.com', {
  transports: ['websocket'],
  autoConnect: false,
})

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      apiResponse:"",
      syncTime:0,
      socketId:null,
      socketConn:false,
      roomId:null,
      roomOwner:true,
      playerSource:'http://www.w3schools.com/html/mov_bbb.mp4',
    };
  }

  handleToggle = () => {
    socket.connected ? socket.close() : this.handleSocket()
    this.setState({socketConn:!this.state.socketConn})
    //setTime([])
  }

  // callAPI(){ // Server handshake
  //   fetch("http://192.168.0.8:3333/testAPI")
  //       .then(res => res.text())
  //       .then(res => this.setState({ apiResponse: res }));
  // }

  handleSocket = () => {
    socket.open()
    socket.on('connect', ()=> {
   // Connected, let's sign-up for to receive messages for this room
    socket.emit('room', room);
    this.setState({roomId:socket.id})
    });

    this.setState({roomId:socket.id})
    //Recieveing Broadcast of TimeSync
    socket.on('playerTime',(timestamp)=>{
      console.log('socketTimestamp: '+timestamp);
    })
    socket.on('playerStop',(timestamp)=>{
      console.log("player stopped at",timestamp);
      this.setState({syncTime:timestamp})
      if(!this.player.paused)
        this.player.pause()
    })
    socket.on('playerStart',(timestamp)=>{
      console.log("player started at",timestamp);
      this.setState({syncTime:timestamp})
      this.player.seek(timestamp)
      if(this.player.paused)
        this.player.play()
      //this.player.play()
    })

  }

  loadVideo=(e)=>{
    e.preventDefault()
    // socket.open()
    // socket.emit("room",this.state.roomId)
    this.setState({playerSource:this.state.selectedFile})
    console.log(this.state.selectedFile);
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.state.playerSource !== prevState.playerSource) {
      this.player.load();
    }
  }
  componentDidMount() {
    this.player.subscribeToStateChange(this.handlePlayerStateChange.bind(this));
    socket.on('connection', (data) => {
      // setTime((prev) => [data, ...prev])
      console.log(data);
    });
    console.log("CombiStream logs!!");
    //Recieveing Broadcast of TimeSync
    socket.on('playerTime',(timestamp)=>{
      //console.log('socketTimestamp: '+timestamp);
      this.setState({syncTime:timestamp})
    })
  }

  handleRoomIdInputChange=(e)=>{
    this.setState({inputPlayerSource:e.target.value})
  }
  onFileChange = event => {
      // Update the state
      var tmppath = URL.createObjectURL(event.target.files[0]);
      this.setState({ selectedFile: tmppath });

    };

  handlePlayerStateChange(state, prevState) {
    // copy player state to this component's state
    this.setState({
      player: state,
      currentTime: state.currentTime
    });
    //console.log("PlayerCurrent Time:", state.currentTime);
    //Emitting player sync timestamp
    // if(this.state.roomOwner)
    socket.emit('playerTime',(state.currentTime))

    if(state.paused!==prevState.paused){
      console.log("player pause:",state.paused);
      // console.log("player useActivity", state.userActivity);
      // console.log("seeking Time:", state.seekingTime);
      if(state.paused)  {
        socket.emit('playerStop',(state.currentTime))
      }
      else {
        socket.emit('playerStart',(state.currentTime))
      }
    }

  }
  render() {

    const CreateRoomBarStyle={
      marginLeft:50,
      textAlign:"left"
    }
    const JoinRoomBarStyle={
      marginLeft:50,
      backGroundColor:"White",
    }

    return (
      <div>
        {/* App bar */}
        <AppBar position="static">
            <Toolbar>
              <Typography variant="h6">
                CombiStream: {this.state.socketConn ? 'Connected !':'Disconnected!!' }
              </Typography>
              <Button onClick={this.handleToggle} style={CreateRoomBarStyle} variant="contained" color="secondary">{this.state.socketConn ? 'Disconnect':'Connect' }</Button>

            </Toolbar>
        </AppBar>

        <p className="App-intro">RoomId: {this.state.roomId}
          {/* <TextField onChange={this.handleRoomIdInputChange} style={JoinRoomBarStyle} name="roomId" id="outlined-basic" label="Enter video URL" variant="outlined" />
          <Button onClick={this.loadVideo} style={JoinRoomBarStyle} variant="contained">Load Video</Button> */}
          <input type="file" onChange={this.onFileChange} />
          <Button onClick={this.loadVideo} style={JoinRoomBarStyle} variant="contained">Load Video</Button>
        </p>
        <br></br>

        <Player
        ref={(player) => { this.player = player }}
        playsInline
        poster="/assets/poster.png"
        src={this.state.playerSource}
        >
          <BigPlayButton position="center" />
        </Player>

      </div>
    );
  }


}

export default App;
